import React, { useContext } from 'react';
import { Card, Pane } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';

function ProductCard({ productToView }) {
  const linkStyles = {
    textDecoration: 'none', // Remove underline
    color: 'inherit', // Inherit the parent's text color
  };

  const {prodView, setProdView} = useContext(GlobalContext);

  const handleClickView = () => {
    setProdView(productToView);
    console.log(prodView)
  }
  return (
    <Link to={`/productView`} style={linkStyles} onClick={handleClickView}>
      <Card
        elevation={1}
        width={300}
        height={300}
        margin={16}
        transition="transform 0.2s"
        cursor="pointer"
        hoverElevation={3}
        onMouseEnter={(e) => {
          e.currentTarget.style.transform = 'scale(1.05)';
        }}
        onMouseLeave={(e) => {
          e.currentTarget.style.transform = 'scale(1)';
        }}
      >
        <Pane
          width="100%"
          height="70%"
          overflow="hidden" // Crop overflow
        >
          <div
            style={{
              width: '100%',
              paddingBottom: '100%', // Maintain aspect ratio
              backgroundImage: `url(${productToView.image_url})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }}
          />
        </Pane>
        <div className='p-2'>
          <h3 className='text-black font-semibold'>{productToView.name}</h3>
          {productToView.is_diskon ? (
            <p>
              <span style={{ textDecoration: 'line-through', color: 'red' }}>
                {productToView.harga_display}
              </span>{' '}
              {productToView.harga_diskon_display}
            </p>
          ) : (
            <p>{productToView.harga_display}</p>
          )}
        </div>
      </Card>
    </Link>
  );
}

export default ProductCard;
