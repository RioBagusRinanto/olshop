import axios from 'axios';
import React, { useContext, useEffect} from 'react'
import { GlobalContext } from '../context/GlobalContext';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Header from './Header';



function ChangeProduct() {
    const {fetchProducts, formData} = useContext(GlobalContext);
    const navigate = useNavigate();
    const token = localStorage.getItem("token");
    const headerparam = {
        Authorization : `Bearer ${token}`
    }
    
      const onSubmit = async (requestInsert) => {
        try {
          console.log(requestInsert)
          const response = await axios.put("https://api-project.amandemy.co.id/api/products/"+formData.id, requestInsert, headerparam);
          console.log(response)
          alert("data sukses dirubah!")
          navigate('/table');
          fetchProducts();
        } catch (error) {
          alert(error.response.data.info)
        } 
      }
      const validateSchema = Yup.object({
        name: Yup.string().required("Nama tidak boleh kosong"),
        harga: Yup.number().typeError("Harga harus angka").required("Harga tidak boleh kosong"),
        stock: Yup.number().typeError("Stok harus angka").required("Stock tidak boleh kosong"),
        image_url: Yup.string().required("Gambar tidak boleh kosong").url("URL tidak valid"),
        is_diskon: Yup.boolean(),
        harga_diskon: Yup.number(),                
        category: Yup.string().required("Kategori tidak boleh kosong"),
        description: Yup.string(),
      });
      
      
      const {values, handleChange,  handleSubmit, errors, handleBlur, touched, resetForm} = useFormik({
        initialValues : {...formData},
        validationSchema : validateSchema,
        onSubmit : onSubmit,
      });
      
    
      const handleCancel = () => {
        resetForm();
      };
      
        
      return (
        <div>
            <Header />
            <div className="container mx-auto p-4">
                <div className="mx-auto bg-white rounded-lg shadow-lg">
                    <h2 className="font-semibold text-cyan-400 text-2xl mb-1 ml-2">Create Product</h2>
                    <div className="grid grid-cols-5 gap-4 p-4">
                    <div className="col-span-3">
                        <label htmlFor="name" className="block text-gray-700 font-semibold">Nama Barang</label>
                        <input onBlur={handleBlur} type="text" value={values.name} id="name" onChange={handleChange} name="name" className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Nama Barang"/>
                        <p className="text-red-600">{touched.name === true && errors.name}</p>
                    </div> 
                    <div className="col-span-2">
                        <label htmlFor="stok" className="block text-gray-700 font-semibold">Stok Barang</label>
                        <input onBlur={handleBlur}  id="stok" value={values.stock} onChange={handleChange} name="stock"  className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Stok Barang"/>
                        <p className="text-red-600">{touched.stock && errors.stock}</p>
                    </div>
                    <div className="col-span-2">
                        <label htmlFor="harga" className="block text-gray-700 font-semibold">Harga Barang</label>
                        <input onBlur={handleBlur}  id="harga" value={values.harga} onChange={handleChange} name="harga"  className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Harga Barang"/>
                        <p className="text-red-600">{touched.harga && errors.harga}</p>

                    </div>
                    <div className="col-span-1">
                        <label htmlFor="kondisi" className="inline-flex items-center">
                        <input type="checkbox" id="kondisi" name="is_diskon" onChange={handleChange} checked={values.is_diskon} className="form-checkbox text-blue-500 rounded" />
                        <span className="ml-2">Status Diskon</span>
                        </label>
                    </div>
                        
                        {values.is_diskon && (  
                          <div className="col-span-2">
                              <label htmlFor="diskon" className="block text-gray-700 font-semibold">
                                  Harga Diskon
                              </label>
                              <input
                                  onBlur={handleBlur}
                                  id="diskon"
                                  onChange={handleChange}
                                  name="harga_diskon"
                                  value={values.harga_diskon}
                                  className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500"
                                  placeholder="Harga Diskon"
                              />
                              <p className="text-red-600">
                              {touched.harga_diskon && errors.harga_diskon}
                            </p>
                          </div>
                        )}
                        
                    <div className="col-span-3">
                        <label htmlFor="kategori" className="block text-gray-700 font-semibold">Kategori Barang</label>
                        <select onBlur={handleBlur}  id="kategori" value={values.category} onChange={handleChange} name="category" className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500">
                        <option value="" disabled>Pilih Kategori Barang</option>
                        <option value="teknologi">Teknologi</option>
                        <option value="makanan">Makanan</option>
                        <option value="minuman">Minuman</option>
                        <option value="hiburan">Hiburan</option>
                        <option value="kendaraan">Kendaraan</option>
                        </select>
                        <p className="text-red-600">{touched.category === true && errors.category}</p>
                    </div>
                    <div className="col-span-3">
                        <label htmlFor="image" className="block text-gray-700 font-semibold">Gambar Barang</label>
                        <input onBlur={handleBlur} type="text" value={values.image_url} onChange={handleChange} id="image_url" name="image_url" className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Image URL"/>
                        <p className="text-red-600">{touched.image_url === true && errors.image_url}</p>
                    </div>
                    <div className="col-span-5">
                        <label htmlFor="deskripsi" className="block text-gray-700 font-semibold">Deskripsi</label>
                        <textarea onBlur={handleBlur} id="deskripsi" value={values.description} onChange={handleChange} name="description" rows="4" className="w-full px-4 py-2 rounded-lg border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Your Message"></textarea>
                        
                    </div>
                    <div className="col-span-5 text-right">
                        <button type="submit" onClick={handleSubmit} className="bg-lime-500 text-white font-semibold px-4 py-2 rounded-lg hover:bg-blue-600 mr-2">Submit</button>
                        <button type="button" onClick={handleCancel} className="bg-gray-300 text-gray-700 font-semibold px-4 py-2 rounded-lg hover:bg-gray-400">Cancel</button>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
      )
}

export default ChangeProduct
