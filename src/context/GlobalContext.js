import axios from "axios";
import { createContext, useEffect, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const initialData = {
    "name": "",
    "harga": 0,
    "stock": 0,
    "image_url": "",
    "is_diskon": false,
    "harga_diskon": 0,
    "category": "",
    "description": ""
  };
  const [products, setProducts] = useState([]);
  const [prodView, setProdView] = useState(initialData);
  const [title, setTitle] = useState("Home Page");

  const fetchProducts = async () => {
    try {
      const response = await axios.get("https://api-project.amandemy.co.id/api/products");
      setProducts(response.data.data);
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  

  const [formData, setFormData] = useState(initialData); // Moved inside the component

  const contextValue = {
    products: products,
    setProducts: setProducts,
    fetchProducts: fetchProducts,
    initialData: initialData,
    formData: formData,
    setFormData: setFormData,
    prodView: prodView,
    setProdView: setProdView,
    title: title,
    setTitle: setTitle
  };

  return (
    <GlobalContext.Provider value={contextValue}>
      {children}
    </GlobalContext.Provider>
  );
};
