import React, { useState, useEffect } from 'react';
import { Pane } from 'evergreen-ui';
import car1 from '../public/img/1.jpg'
import car2 from '../public/img/2.jpg'
import car3 from '../public/img/3.jpg'

const images = [
  car1,
  car2,
  car3,
]; // Replace with your image URLs

function SlideShow() {
  const [currentImage, setCurrentImage] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImage((prevImage) => (prevImage + 1) % images.length);
    }, 10000); // Change slide every 10 seconds (adjust as needed)

    return () => clearInterval(interval);
  }, []);

  return (
    <Pane width="100%" height={400} overflow="hidden">
      {images.map((image, index) => (
        <Pane
          key={index}
          display={index === currentImage ? 'block' : 'none'}
          backgroundImage={`url(${image})`}
          backgroundSize="cover"
          backgroundPosition="center"
          width="100%"
          height="100%"
        />
      ))}
    </Pane>
  );
}

export default SlideShow;
