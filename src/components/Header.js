import React from 'react';
import { Pane, Link as ELink,Text, Button } from 'evergreen-ui';
import { Link as RouterLink, useNavigate} from 'react-router-dom';
import axios from 'axios';

function Header() {
    const token = localStorage.getItem("token");
    const validateToken = token !== null;
    const username = localStorage.getItem("user");
    const navigate = useNavigate();
    const linkStyle = {
        color : 'white',
        textDecoration : 'none',
        fontSize : 18
    }
    const headerStyle = {
        position: 'fixed', // Make the header fixed
        top: 0, // Stick it to the top
        left: 0,
        right: 0,
        zIndex: 999, // Adjust z-index as needed
        background: 'blue',
        color: 'white'
    };
    const handleLogout = () => {
      const headerBearer = {
        Authorization : `Bearer ${token}`
      }
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      localStorage.removeItem('userData');
      axios.post('https://api-project.amandemy.co.id/api/logout', null, headerBearer).then((response) => {

        console.log(response);
      }).catch((error) => {
        console.error('Error : ',error);
      });
      navigate("/");
    }
    
  return (
    <Pane
      position="fixed"
      display="flex"
      alignItems="center"
      justifyContent="space-between"
      padding={16}
      borderBottom="default"
      background='blue'
      color='white'
      style={headerStyle}
    >
      <Text fontSize={24} fontWeight="bold" style={linkStyle} >
        Ini Olshop
      </Text>
      <Pane>
        <ELink marginRight={20} href="/" style={linkStyle}>
          Home
        </ELink>
        <ELink marginRight={20} href="/products" style={linkStyle}>
          Products
        </ELink>
        {
          validateToken ? (
            <>
              <ELink href="/table" style={linkStyle}>Table</ELink>
            </>
          ) : (
            <></>
          )
        }
      </Pane>
      <Pane>
      {validateToken ? (
          <>
            <h3 className='font-semibold capitalize'>{username}  
              <Button  onClick={handleLogout}>
                Logout
              </Button>
            </h3>
            
          </>
        ) : (
          <>
            <RouterLink to="/login" style={linkStyle} className="mr-2">
              <Button size="medium" appearance="secondary">
                Login
              </Button>
            </RouterLink>
            <RouterLink to="/register" style={linkStyle}>
              <Button size="medium">Register</Button>
            </RouterLink>
          </>
        )}
      </Pane>
    </Pane>
  );
}

export default Header;
