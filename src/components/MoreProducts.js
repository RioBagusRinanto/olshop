import React, { useContext, useEffect } from 'react'
import { GlobalContext } from '../context/GlobalContext'
import Header from './Header'
import ProductList from './ProductList'
import { Helmet } from 'react-helmet';

function MoreProducts() {
    const {products, title, setTitle} = useContext(GlobalContext);
    useEffect(() => {
      setTitle("Product List");
    })
    return (
      <div>
          <Header />
          <Helmet>
            <title>{title}</title>
          </Helmet>
          <div className="flex justify-between">
              <h1 className="py-2 capitalize text-blue-600 font-mono text-xl font-bold">
              Catalog Products
              </h1>
          </div>
          <ProductList productList = {products}  />
      </div>
      )
}

export default MoreProducts