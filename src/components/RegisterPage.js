import React, { useContext, useEffect, useState } from 'react'
import { Pane, Text, TextInput, Button } from 'evergreen-ui';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';
import { Helmet } from 'react-helmet';


function RegisterPage() {
  const {title, setTitle} = useContext(GlobalContext);
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    name: '',
    username: '',
    email: '',
    password: '',
    password_confirmation: '',
  });
  useEffect(() => {
    setTitle("Register");
  })

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Send a POST request to the API
      const response = await axios.post(
        'https://api-project.amandemy.co.id/api/register',
        formData
      );

      // Handle the response as needed
      console.log('Registration successful', response.data);
      navigate("/login");
    } catch (error) {
      // Handle errors, e.g., display an error message
      console.error('Registration failed', error);
    }
  };
    
      return (
        <Pane
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100vh"
    >
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <form
        onSubmit={handleSubmit}
        style={{
          width: 300,
          padding: 24,
          backgroundColor: 'white',
          borderRadius: 8,
          boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.2)',
          textAlign: 'center',
        }}
      >
        <Text fontSize={24} marginBottom={16} fontWeight="bold">
          Register
        </Text>
        <TextInput
          name="username"
          value={formData.username}
          onChange={handleInputChange}
          placeholder="Username"
          marginBottom={16}
          width="100%"
          marginTop={16}
        />
        <TextInput
          name="name"
          value={formData.name}
          onChange={handleInputChange}
          placeholder="Name"
          marginBottom={16}
          width="100%"
        />
        <TextInput
          name="email"
          value={formData.email}
          onChange={handleInputChange}
          placeholder="Email"
          marginBottom={16}
          width="100%"
        />
        <TextInput
          name="password"
          value={formData.password}
          onChange={handleInputChange}
          type="password"
          placeholder="Password"
          marginBottom={16}
          width="100%"
        />
        <TextInput
          name="password_confirmation"
          value={formData.password_confirmation}
          onChange={handleInputChange}
          type="password"
          placeholder="Confirmation"
          marginBottom={16}
          width="100%"
        />
        <Button type="submit" appearance="primary" height={40} width="100%">
          Register
        </Button>
      </form>
    </Pane>
      );
    
}

export default RegisterPage