import React, { useContext, useEffect } from 'react'
import Header from './Header'
import SlideShow from './SlideShow'
import ProductList from './ProductList'
import { Button } from 'evergreen-ui'
import { GlobalContext } from '../context/GlobalContext'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

function LandingPage() {
    const {products, title, setTitle} = useContext(GlobalContext);
    const publicProd = products.slice(0,4);
    useEffect(() => {
      setTitle("Home Page");
    })
  return (
    <div>
        <Helmet>
          <title>{title}</title>
        </Helmet>
        <Header />
        <SlideShow />
        <div className="flex justify-between">
            <h1 className="py-2 capitalize text-blue-600 font-mono text-xl font-bold">
            Catalog Products
            </h1>
            <Link to={"/products"} className='mt-4 mr-4'>
                <Button backgroundColor='transparent' borderColor='blue' color='blue' height={40}>See More</Button>
            </Link>
        </div>
        <ProductList productList = {publicProd}  />
    </div>
  )
}

export default LandingPage