import React, { useContext, useEffect } from 'react'
import { GlobalContext } from '../context/GlobalContext'
import Header from './Header';
import { Helmet } from 'react-helmet';

function ProductDetail() {
    const {prodView, title, setTitle} = useContext(GlobalContext);
    console.log(prodView)
    useEffect(() => {
        setTitle(prodView.name);
    }, [])
  return (
    <div>
        <Header />
        <Helmet>
            <title>{title}</title>
        </Helmet>
        <div className="container mx-auto p-4 mt-14">
            <div className="mx-auto bg-white rounded-lg shadow-lg">
                <div className="flex flex-col md:flex-row">
                    <div className="md:w-1/2 p-4">
                        <img src={prodView.image_url} alt="Product" className="w-full h-auto rounded-lg object-contain"/>
                    </div>
                    <div className="md:w-1/2 p-4 mt-4">
                        <h2 className="text-4xl font-bold">{prodView.name}</h2>
                        <h2 className="text-md text-gray-400 font-mono">{prodView.category}</h2>
                        {prodView.is_diskon ? (
                            <div>
                                <h2 className="text-xl font-bold line-through mt-10">{prodView.harga_display}</h2>
                                <h2 className="text-3xl font-bold text-red-500 mb-10">{prodView.harga_diskon_display}</h2>
                            </div>
                        ): (
                            <div>
                                <h2 className="text-3xl font-bold mb-10">{prodView.harga_display}</h2>
                            </div>
                        )}
                        <p className="text-gray-600 mt-2">{prodView.description}</p>
                        <p className="text-blue-500 mt-4 font-semibold">Stock: {prodView.stock}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ProductDetail