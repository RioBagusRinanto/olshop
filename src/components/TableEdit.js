import React, { useContext, useEffect } from 'react';
import Header from './Header';
import { GlobalContext } from '../context/GlobalContext';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Helmet } from 'react-helmet';

function TableEdit() {
  const navigate = useNavigate();
  const token = localStorage.getItem('token');
  
  const validateToken = token !== null;
  if(!validateToken){
    navigate("/login");
  }
  const { products, setFormData, fetchProducts, title, setTitle, setProducts } = useContext(GlobalContext);
  const headerBearer = {
    Authorization : `Bearer ${token}`
  }
  const prd = async () => {
    const prd1 = await axios.get("https://api-project.amandemy.co.id/api/final/products", headerBearer).then((response) => {
      setProducts(response.data.data);
    });

  }
  useEffect(() => {
    setTitle("Table Edit");
  })
  const allowedKeys = ["id", "name", "is_diskon", "harga", "harga_diskon", "image_url", "category"];
  const deleteProduct = async (id) => {
    try {
      const rsp = await axios.delete("https://api-project.amandemy.co.id/api/products/" + id);
      
      fetchProducts();
    } catch (error) {
      alert(error);
    }
  };
  const handleUpdate = (item) => {
    setFormData(item);
    navigate("/update");
  };
  const handleDelete = (item) => {
    deleteProduct(item.id);
  };

  
  return (
    <div className="flex flex-col">
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Header />
      <div className="flex-grow p-4 overflow-x-auto mt-16">
        <div className="mb-4">
          <Link to="/add">
            <button className="px-3 py-2 bg-green-500 text-white rounded mb-4 sm:mb-0">
              Add New Item
            </button>
          </Link>
        </div>
        <table className="min-w-full border divide-y divide-gray-200">
                <thead className="bg-gray-100">
                <tr>
                    {allowedKeys.map((key) => {
                    let columnHeader = key;
                    if (key === "is_diskon") {
                        columnHeader = "status diskon";
                    } else if (key === "harga_diskon") {
                        columnHeader = "harga diskon";
                    } else if (key === "image_url") {
                        columnHeader = "gambar";
                    }

                    return (
                        <th
                        key={key}
                        className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                        {columnHeader}
                        </th>
                    );
                    })}
                    <th
                    className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                    Action
                    </th>
                </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                {products.map((item, rowIndex) => (
                    <tr key={rowIndex}>
                    {allowedKeys.map((key) => {
                        let cellValue = item[key];
                        if (key === "is_diskon") {
                        cellValue = item[key] ? "Aktif" : "Mati";
                        } else if (key === "image_url") {
                            cellValue = (
                                <div className="flex justify-center items-center">
                                <img src={item[key]} alt="Product" className="w-16 h-auto" />
                                </div>
                            );
                        }
                        return (
                        <td
                            key={key}
                            className="px-6 py-4 whitespace-nowrap"
                        >
                            {cellValue}
                        </td>
                        );
                    })}
                    <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex gap-2 items-center justify-center">
                        <button className="px-3 py-2 bg-blue-500 text-white rounded" onClick={() => handleUpdate(item)}>Update</button>
                        <button className="px-3 py-2 bg-red-500 text-white rounded" onClick={() => handleDelete(item)}>Delete</button>
                        </div>
                    </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    </div>
  )

}

export default TableEdit