import React, { useContext, useEffect, useState } from 'react';
import { Pane, Text, TextInput, Button } from 'evergreen-ui';
import axios from 'axios';
import { GlobalContext } from '../context/GlobalContext';
import { Helmet } from 'react-helmet';
import { useNavigate } from 'react-router-dom';

function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {title, setTitle} = useContext(GlobalContext);
  useEffect(() => {
    setTitle("Login");
  })
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('https://api-project.amandemy.co.id/api/login', {
        email,
        password,
      });

      localStorage.setItem('userData', response.data.data.user);
      localStorage.setItem('token', response.data.data.token);
      localStorage.setItem('user', response.data.data.user.username);
      console.log('Login successful', response.data);
      navigate("/");
    } catch (error) {
      console.error('Login failed', error);
    }
  };

  return (
    <Pane
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100vh"
    >
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <form
        onSubmit={handleSubmit}
        style={{
          width: 300,
          padding: 24,
          backgroundColor: 'white',
          borderRadius: 8,
          boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.2)',
          textAlign: 'center',
        }}
      >
        <Text fontSize={24} marginBottom={16} fontWeight="bold">
          Login
        </Text>
        <TextInput
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Email"
          marginBottom={16}
          width="100%"
          marginTop={16}
        />
        <TextInput
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="Password"
          marginBottom={16}
          width="100%"
        />
        <Button type="submit" appearance="primary" height={40} width="100%">
          Login
        </Button>
      </form>
    </Pane>
  );
}

export default LoginPage;
