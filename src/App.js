
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LandingPage from "./components/LandingPage";
import LoginPage from "./components/LoginPage";
import MoreProducts from "./components/MoreProducts";
import ProductDetail from "./components/ProductDetail";
import TableEdit from "./components/TableEdit";
import RegisterPage from "./components/RegisterPage";
import { useContext } from "react";
import { Helmet } from "react-helmet";
import { GlobalContext } from "./context/GlobalContext";
import AddProduct from "./components/AddProduct";
import ChangeProduct from "./components/ChangeProduct";

function App() {
  const {title} = useContext(GlobalContext);
  
  return (
    <div className="App">
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/products" element={<MoreProducts />} />
          <Route path="/productView" element={<ProductDetail />} />
          <Route path="/table" element={<TableEdit /> } />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/add" element={<AddProduct />} />
          <Route path="/update" element={<ChangeProduct />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
