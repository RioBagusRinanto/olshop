import React from 'react'
import ProductCard from './ProductCard';


function ProductList({productList}) {
  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        padding: '16px',
      }}
    >
      {productList.map((product, index) => (
        <ProductCard key={index} productToView={product} />
      ))}
    </div>
  )
}

export default ProductList